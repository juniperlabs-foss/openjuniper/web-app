# OpenJuniper 

### Developing

* Requires [Node](https://nodejs.org/en/) - Recommend latest LTS version (v12.4.1 at the time of writing)
* Requires [NPM](https://github.com/npm/cli)

Get them both with Node Version Managed [NVM](https://github.com/nvm-sh/nvm)

```bash
# This may not be the latest link DOUBLE CHECK at https://github.com/nvm-sh/nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash

# Install the latest LTS
nvm install --lts

# Double Check
node -v
npm -v
```

### Run (for development purposes):

```bash
npm install
npm start
```

### Hosting

TLDR; This is a pretty basic site, comprised of some HTML, CSS, JS and Images. It can be hosted easily 
by a simple web server (for instance Apache, Nginx) running on your favorite server. 

Alternatively you can use your favorite web hosting provider. Below are some options

#### AWS

To host a static web site with AWS all you need is a properly configured [S3 bucket](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)

To make it really sing though you'll need to leverage a couple more services. In total that's:

* S3 - provides the 'source of truth' for your website, where the source files are stored (in a bucket) and served from. Plus a six sigma uptime guarantee and a wicked low cost to operate 
* Certificate Manager - the easiest way to get https:// support for your site within the AWS suite
* Cloudfront - AWS CDN service - S3 does not support https static websites and / or http > https redirects. We leverage Cloudfront to accomplish this as well as all the benefits a CDN provides (caching of website resources, reduced network latency, etc)
 

Some things to note: 

*S3*

* S3 is hosting the site (server side storage plus static file web server)
* You can hit the website via the S3 bucket - [http://www.openjuniper.com.s3-website-us-west-2.amazonaws.com/](http://www.openjuniper.com.s3-website-us-west-2.amazonaws.com/), this can be turned off but from a security standpoint is fine.
* You turn on / off website hosting via the S3 console `bucket > properties > static website hosting`, this is also where the S3 public URL is
* The bucket is open to the public *DONT PUT ANYTHING IN IT YOU WOULDN'T WANT THE WORLD TO KNOW ABOUT*
* You configure bucket security settings via the bucket policy `bucket > permissions > bucket policy`

*Certificate Manager*

* CM is the AWS service that provisions (and manages) SSL certificates. There is no charge for this service. 
* There are two parts 1) the certificate request and then 2) domain validation. You request the certificate via the CM console and then prove ownership either via email or DNS. The latter is preferred as it allows for auto-updating the SSL cert every year. There is a CNAME record that has been added to the domain to allow for DNS validation
* Due to AWS peculiarities the CM certificate for Cloudfront distributions must reside in the N. Virginia region. Use the Region drop down in AWS to manage the CM certificates

*Cloudfront*

* CDN service with support for SSL and HTTP > HTTPS redirects.
* What [Cloudfront is](https://aws.amazon.com/cloudfront/)

*Deploying to AWS (S3)*

* Login to the AWS Console. Navigate to S3
* Select the OpenJuniper bucket `www.openjuniper.com`
* Select All Bucket Objects > Delete (from Actions Drop Down) > Confirm (Obviously please make sure your ready before deleting the content)
* Select Upload > Upload the entire contents of the `src/` directory. The uploaded directory structure should resemble:

```bash
-assets
index.html
```

_That Complete's the Deployment_

*Troubleshooting

Caching - the most common issue that may arise is the CDN (Cloudfront) servers are not up-to-date with what is stored in the S3 bucket. This is a caching issue. You can likely solve the problem with a hard refresh of your browser but a better solution is to invalidate the Cloudfront cache. You do this by:

* Login to the AWS Console. Navigate to Cloudfront
* Select the OpenJuniper distribution
* Select `Invalidations` from the tabs at the top
* Select a recent invalidation and select copy and invalidate

If there are no invalidations available create a new one, specifying all object paths. Similar to:

```bash
/assets/*
/index.html
```
